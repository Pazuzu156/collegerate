<?php

class RatingController extends Controller
{
	public function uploadImage()
	{
		$valid = Validator::make(Input::all(), array(
			'college' 	=> 'required',
			'year' 		=> 'required'
		));

		// echo "<pre>", print_r(Input::all()), "</pre>";
		// die();

		if($valid->fails())
			return Redirect::route('home')->withErrors($valid)->withInput()->with('emsg', 'Invalid Form Submission');

		$imgDir = Config::get('collegerate.upload_dir');

		// Encoding image name for the name of the image
		// this will be appended to the image name to be
		// stored in the database
		$hash = Hash::make(str_random(60));

		// Check file has been uploaded successfully
		if(Input::hasFile('image'))
		{
			$allowed_extensions = array('jpg','jpeg','gif','png','tiff', 'bmp'); // array holding allowed extensions for filtering
			$upload_extension = Input::file('image')->getClientOriginalExtension(); // Get file's extension
			if(!in_array($upload_extension, $allowed_extensions))
				return Redirect::route('home')->withInput()->with('emsg', 'Invalid filetype. Please upload only images!');

			$uploadedImageName = base64_encode($hash) . '_' . Input::file('image')->getClientOriginalName();
			if(Input::file('image')->move($imgDir, $uploadedImageName))
			{
				$image = Image::create(array(
					'image_name' => $uploadedImageName,
					'school' => Input::get('college'),
					'year' => Input::get('year')
				));

				if($image)
					return Redirect::route('home')->with('smsg', 'Image has successfully uploaded. Why not rate other images now?');
			}
		}
		else
		{
			return Redirect::route('home')->with('emsg', 'You must select an image to upload!')->withInput();
		}

		return Redirect::route('home')->with('emsg', 'There was an error uploading your image. Please try again later')->withInput();
	}

	public function submitRating()
	{
		$valid = Validator::make(Input::all(), array(
			'image_name' => 'required|exists:images'
		));

		if($valid->fails())
			return Redirect::route('rate')->withErrors($valid)->with('emsg', 'That image was not found in the database!');

		$image = Image::where('image_name', '=', Input::get('image_name'))->first();

		Rating::create(array(
			'image_id' => $image->id,
			'rating' => Input::get('rate')
		));

		return Redirect::route('rate');
	}
}
