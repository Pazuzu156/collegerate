<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CollegeRate - {{ $title }}</title>
	<meta charset="utf-8">
	{{ HTML::style('css/global.css') }}
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<a href="./"><img src="//localhost:8000/images/logo.png" alt="Logo"></a>
		</div>
		<div id="pageContent">
			@yield('content')
		</div>
		<div style="height: 100px; clear: both;"></div>
		<div id="footer">
			<div id="footContent">
				<span id="copy">&copy; <?php echo date("Y"); ?> CollegeRate Inc. - All Rights Reserved</span><br>
		        <a href="#">Feedback</a> |
		        <a href="#">Privacy Policy</a> |
		        <a href="#">Terms of Use</a> |
		        <a href="#">Disclaimer</a>
			</div>
		</div>
	</div>
</body>
</html>
