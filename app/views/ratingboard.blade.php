@extends('layout.main')
@section('content')
<h1 style="text-align: center;">Start Rating!</h1>
<div class="thumb">
	@if($errors->has('image_name'))
		<span class="error">{{ $errors->first('image_name') }}</span><br>
	@endif
	<img src="{{ URL::asset('uploaded_images') . '/' . $image->image_name }}" alt="Rate!!">
	<p>
		From: {{ CollegeRate\CRExtras::getSchool($image->school) }}
		|
		Year: {{ CollegeRate\CRExtras::getYear($image->year) }}
	</p>
	<div id="rate">
		Rate Here:
		{{ Form::open(array('action' => 'submit-rating')) }}
		@for($i = 0; $i < 5; $i++)
		{{ Form::submit($i+1, array('class' => 'rate', 'name' => 'rate')) }}
		@endfor
		{{ Form::hidden('image_name', $image->image_name) }}
		{{ Form::close() }}
	</div>
</div>
@stop
