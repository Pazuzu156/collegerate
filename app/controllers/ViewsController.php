<?php

use CollegeRate\CRExtras;

class ViewsController extends Controller
{
	public function getIndex()
	{
		return View::make('home')->with('title', 'Home');
	}

	public function getRatingBoard()
	{
		$count = json_encode(Image::all()->count()); // Get number of images
		if($count > 0)
		{
			$randomID = rand(1, $count);
			$randomImage = Image::find($randomID);

			return View::make('ratingboard')->with('title', 'Rate')->with('image', $randomImage);
		}

		return Redirect::route('home')->with('emsg', 'There are currently no images uploaded to rate. Try uploading some!');
	}
}
