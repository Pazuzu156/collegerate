# CollegeRate

## Requirements
[Git](http://git-scm.com)  
[Composer](https://getcomposer.org)  
[Web Server](https://www.apachefriends.org/index.html) I prefer XAMPP

Afterwords, you're all set. Simple clone the repo into the htdocs directory of XAMPP:
```
$ git clone https://bitbucket.org/Pazuzu156/CollegeRate.git
```

Once you've cloned the repo, install Laravel's required components using Composer:
```
$ composer install
```

You MUST be inside the root directory of CollegeRate to use composer

After Laravel is installed, you must edit a few things to get it working on the local server:
```php
Under bootstrap/start.php
-------------------------
Edit the line:
'local' => array('homestead', 'Kaleb-WIN64'),
Adding your computer's name after mine

Under app/config/local/database.php OR app/config/database.php if you are pushing to live server:
-------------------------------------------------------------------------------------------------
Edit:
'mysql' => array(
	'driver'    => 'mysql',
	'host'      => 'localhost',
	'database'  => 'collegerate_testdb',
	'username'  => 'root',
	'password'  => '',
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => '',
),
To fit your database needs

Under app/config/local/app.php OR app/config/app.php if you are pushing to live server:
---------------------------------------------------------------------------------------
Edit the line:
'url' => 'http://localhost:8000',
To reflect the URL you will be using
```

Finally, you are done configuring. Now, we need to migrate the databases. Just run:
```
$ php artisan migrate
```

And you're all done. Now start the web server and the MySQL server and enjoy!
