<?php

class Rating extends Eloquent
{
	protected $fillable = array('image_id', 'rating');
	protected $table = 'ratings';
}
