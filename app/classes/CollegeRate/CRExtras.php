<?php namespace CollegeRate;

class CRExtras
{
	public static function getSchool($schoolAbbr)
	{
		$schools = array(
			'su'	=> 'Stanford University',
			'uga'	=> 'University of Georgia',
			'ufl'	=> 'University of Florida',
			'hu'	=> 'Harvard University',
			'penn'	=> 'Pensylvania State University',
			'uwi'	=> 'University of Wisconson',
			'ucf'	=> 'University of Centeral Florida',
			'uca'	=> 'University of California',
			'umi'	=> 'University of Mississippi',
			'uco'	=> 'University of Colorado',
			'utx'	=> 'University of Texas',
			'asu'	=> 'Arizona State University',
			'ou'	=> 'Ohio University'
		);

		return $schools[$schoolAbbr];
	}

	public static function getYear($yearAbbr)
	{
		$years = array(
			'fresh'	=> 'Freshman',
			'soph'	=> 'Sophmore',
			'jun'	=> 'Junior',
			'sen'	=> 'Senior'
		);

		return $years[$yearAbbr];
	}
}
