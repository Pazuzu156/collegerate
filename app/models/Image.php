<?php

class Image extends Eloquent
{
	protected $fillable = array('image_name', 'school', 'year');
	protected $table = 'images';
}
