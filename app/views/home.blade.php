@extends('layout.main')
@section('content')
{{ Form::open(array('action' => 'submit-image', 'files' => true)) }}

<table id="tblUp">
	<tr>
		@if(Session::has('emsg'))
		<td class="cell">
			<span class='error'>{{ Session::get('emsg') }}</span><br>
			@foreach($errors->all() as $error)
				{{ $error }}<br>
			@endforeach
		</td>
		@elseif(Session::has('smsg'))
		<td class="cell">
			<span class='success'>{{ Session::get('smsg') }}</span>
		</td>
		@endif
	</tr>
	<tr>
		<td class="cell">
			<h2 style="text-align: center;">Upload an image now!</h2>
			Select Image File:
			{{ Form::file('image', array('id' => 'image')) }}
			{{ Form::select('college', array(
				'' 	=> '-- Select College--',
				'su' 		=> 'Stanford University',
				'uga' 		=> 'University of Georgia',
				'ufl' 		=> 'University of Florida',
				'hu' 		=> 'Harvard University',
				'penn' 		=> 'Pennsylvania State University',
				'uwi' 		=> 'University of Wisconson',
				'ucf' 		=> 'University of Central Florida',
				'uca' 		=> 'University of California',
				'umi' 		=> 'University of Mississippi',
				'uco' 		=> 'Universiry of Colorado',
				'utx' 		=> 'University of Texas',
				'asu' 		=> 'Arizona State University',
				'ou' 		=> 'Ohio University'
			)) }}
			{{ Form::select('year', array(
				'' 	=> '-- Year --',
				'fresh' 	=> 'Freshman',
				'soph' 		=> 'Sophmore',
				'jun' 		=> 'Junior',
				'sen' 		=> 'Senior'
			)) }}
			<br>
			<div style="text-align: center;">{{ Form::submit('Upload Image', array('id' => 'upload')) }}</div>
		</td>
	</tr>
	<tr>
		<td class="cell">
			{{ HTML::linkRoute('rate', 'Start Rating Now')}}
		</td>
	</tr>
</table>

{{ Form::close() }}
@stop
