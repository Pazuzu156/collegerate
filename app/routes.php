<?php

Route::get('/', array(
	'as' => 'home',
	'uses' => 'ViewsController@getIndex'
));

Route::get('/rate', array(
	'as' => 'rate',
	'uses' => 'ViewsController@getRatingBoard'
));

Route::group(array('before' => 'csrf'), function()
{
	Route::post('/submit/image', array(
		'as' => 'submit-image',
		'uses' => 'RatingController@uploadImage'
	));

	Route::post('/submit/rating', array(
		'as' => 'submit-rating',
		'uses' => 'RatingController@submitRating'
	));
});
